
  const initialState = {
    searchedAlbums: [],
    album: ''
  };
  
  const music = (state = initialState, action) => {
    const { type, searchedAlbums, searchedMoreAlbums, album } = action;
    switch (type) {
      case 'SET_SEARCHED_ALBUMS':
        return { ...state, searchedAlbums };
      case 'SET_SEARCHED_MORE_ALBUMS':
        return { ...state, searchedAlbums: state.searchedAlbums.concat(searchedMoreAlbums) };
      case 'SET_ALBUM': 
        return { ...state, album }
      default:
        return state;
    }
  };
  
  export default music;
  