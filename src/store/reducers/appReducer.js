
  const initialState = {
    loading: false,
    error: "",
    message: "",
  };
  
  const app = (state = initialState, action) => {
    const { type } = action;
    switch (type) {
      default:
        return state;
    }
  };
  
  export default app;
  