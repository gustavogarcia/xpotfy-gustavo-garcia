import axios from 'axios';
import { millisToMinutes } from '../../helpers'

export const refreshToken = () => {
  const token = window.localStorage.getItem('token')
  const api = axios.create({
    baseURL: process.env.API_URL,
    headers: {
      'authorization': `Bearer ${token}`
    },
  });

  token && api.get(`me`)
    .then(res => console.log(res))
    .catch(error => {
      console.log(error)
      switch (error.response.status) {
        case 400:
        case 401:
          window.localStorage.removeItem('token');
          return authorizeAPI();
        default:
          break;
      }
    })
}

export const authorizeAPI = () => {
  const url = `${process.env.AUTHORIZE_URL}/authorize`
  const clientID = `client_id=${process.env.CLIENT_ID}`
  const redirect_uri = `redirect_uri=${process.env.REDIRECT_URI}`
  const scope = `scope=${process.env.SCOPES}`
  const responseType = `response_type=token`
  window.location = `${url}/?${clientID}&${redirect_uri}&${scope}&${responseType}`
}

export const searchAlbums = query => dispatch => {
  const token = window.localStorage.getItem('token')
  const api = axios.create({
    baseURL: process.env.API_URL,
    headers: {
      'authorization': `Bearer ${token}`
    },
  });

  api
    .get(`/search/?q=${query}&type=album`)
    .then(({ data: { albums } }) => {
      const searchedAlbums = albums.items.map(album => {
        return {
          albumID: album.id,
          albumCover: album.images[0].url,
          albumName: album.name,
          artistName: album.artists.map(artist => artist.name).join(", ")
        }
      })
      window.localStorage.setItem('recentSearch', JSON.stringify(searchedAlbums.filter((item, index) => index < 6)));
      dispatch({ type: "SET_SEARCHED_ALBUMS", searchedAlbums })
    })
    .catch(error => {
      console.log(error)
      switch (error.response.status) {
        case 400:
        case 401:
          window.localStorage.removeItem('token');
          return authorizeAPI();
        default:
          break;
      }
    })
};

export const searchMoreAlbums = (query, times) => dispatch => {
  const offstet = times * 20
  const token = window.localStorage.getItem('token')
  const api = axios.create({
    baseURL: process.env.API_URL,
    headers: {
      'authorization': `Bearer ${token}`
    },
  });

  api
    .get(`/search/?q=${query}&type=album&offset=${offstet}`)
    .then(({ data: { albums } }) => {
      const searchedMoreAlbums = albums.items.map(album => {
        return {
          albumID: album.id,
          albumCover: album.images[0].url,
          albumName: album.name,
          artistName: album.artists.map(artist => artist.name).join(", ")
        }
      })
      dispatch({ type: "SET_SEARCHED_MORE_ALBUMS", searchedMoreAlbums })
    })
    .catch(error => {
      console.log(error)
      switch (error.response.status) {
        case 400:
        case 401:
          window.localStorage.removeItem('token');
          return authorizeAPI();
        default:
          break;
      }
    })
};

export const getAlbum = albumID => dispatch => {
  const token = window.localStorage.getItem('token')
  const api = axios.create({
    baseURL: process.env.API_URL,
    headers: {
      'authorization': `Bearer ${token}`
    },
  });

  api
    .get(`/albums/${albumID}`)
    .then(({ data: { artists, name, tracks, images } }) => {
      const album = {
        albumID,
        albumCover: images[0].url,
        albumName: name,
        artistName: artists.map(artist => artist.name).join(", "),
        albumSongs: tracks.items.map(({ name, track_number, duration_ms, id, preview_url }) => {
          return {
            songID: id,
            songTrack: track_number,
            songName: name,
            songTime: millisToMinutes(duration_ms),
            songPreview: preview_url
          }
        }),
      }

      const recentOpen = JSON.parse(window.localStorage.getItem('recentOpen')) || [];
      (!recentOpen[0] || recentOpen[0].albumID !== albumID) && recentOpen.unshift(album)
      window.localStorage.setItem('recentOpen', JSON.stringify(recentOpen.filter((item, index) => index < 6)))
      dispatch({ type: "SET_ALBUM", album })
    })
    .catch(error => {
      switch (error.response.status) {
        case 400:
        case 401:
          window.localStorage.removeItem('token');
          authorizeAPI();
        default:
          break;
      }
    })
}

export const unsetAlbum = () => dispatch => {
  dispatch({ type: "SET_ALBUM", album: "" })
}

export const unsetAlbums = () => dispatch => {
  dispatch({ type: "SET_SEARCHED_ALBUMS", searchedAlbums: [] })
}