import React from 'react';
import { useHistory } from "react-router-dom";

export default function Logo() {
    let history = useHistory();

    return (
        <button className="logo" onClick={() => history.push('/')}><h1><span>XP</span>otfy</h1></button>
    )
}