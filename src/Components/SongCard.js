import React from 'react';
import { useTranslation } from 'react-i18next';

export default function SongCard({ songTrack, songName, songTime, songPreview, selectedTrack, setSelectedTrack, player, playerStatus }) {
  const { t } = useTranslation();
  const isSelected = selectedTrack === songPreview;
  const isPlaying = isSelected && playerStatus === 'playing';
  const playerButtonClasses = `playerButton ${isSelected ? playerStatus : ''}`;
  return (
    <li className={`songCard ${isSelected ? 'selected' : ''}`}>
      <span className="songTrack">{songTrack}.</span>
      <span className="songName">{songName}</span>
      <span className="songTime">{songTime}</span>
      { songPreview 
        && ( isPlaying
          ? <button className={playerButtonClasses} onClick={() => player.current.pause()}/>
          : <button className={playerButtonClasses} onClick={() => {setSelectedTrack(songPreview); player.current.play() }}/>
        )}
    </li>
  )
} 