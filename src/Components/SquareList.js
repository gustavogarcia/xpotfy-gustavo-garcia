import React from 'react';
import { useTranslation } from 'react-i18next';
import AlbumCard from './AlbumCard';

export default function SquareList({ label, albums, query }) {
  const { t } = useTranslation();

  return (
    <div className="squareList">
      <h2 className="listLabel">{`${t(label)}${query ? ` "${query}"` : ''}`}</h2>
      <ul className="albumList">
        {albums && albums.map((album,index) => <AlbumCard key={`album.albumID${index}`} {...album}/>)}
      </ul>
    </div>
  )
} 