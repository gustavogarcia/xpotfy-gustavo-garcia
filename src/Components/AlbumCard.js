import React from 'react';
import { Link } from 'react-router-dom';
import { goToPage } from '../helpers'
import AlbumCover from './AlbumCover'

export default function AlbumCard({ albumCover, albumName, artistName, albumID }) {
  const linkToAlbum = { pathname: goToPage('album', albumName, artistName), state: { albumID }};

  return (
    <>
      <li className="albumCard">
          <Link to={linkToAlbum}><AlbumCover albumCover={albumCover}/></Link>
          <Link to={linkToAlbum}><span className="albumName">{albumName}</span></Link>
          <Link to={goToPage('artist', artistName)}><span className="artistName">{artistName}</span></Link>
      </li>
    </>
  )
} 