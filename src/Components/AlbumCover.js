import React from 'react';
import placeholder from '../images/spotfy.svg';

export default function AlbumCover({ albumCover }) {

  return (
    <img className={`albumCover ${albumCover ? '' : 'placeholder'}`} src={albumCover ? albumCover : placeholder} />
  )
} 