import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

export default function SearchPage({className, label, placeholder, value, onChange }) {
  const { t } = useTranslation();
  const [ fieldOnFocus, setFieldOnFocus ] = useState(false)

  return (
    <div className={`field ${className} ${fieldOnFocus ? "fieldOnFocus" : ""}`}>
      <label>{t(label)}</label>
      <input 
        placeholder={t(placeholder)} 
        value={value} 
        onFocus={(() => setFieldOnFocus(true))} 
        onBlur={(() => setFieldOnFocus(false))} 
        onChange={({target: { value}}) => onChange(value)}
      />
    </div>
  )
} 