import React from 'react';
import braFlag from '../images/brasil.png';
import usaFlag from '../images/usa.png';
import { useTranslation } from 'react-i18next';

export function Flags() {
  const [t, i18n] = useTranslation();

  return (
    <div className="flags">
      <img onClick={() => i18n.changeLanguage('pt') } src={braFlag} className="flag" />
      <img onClick={() => i18n.changeLanguage('en') } src={usaFlag} className="flag" />
    </div>
  )
}