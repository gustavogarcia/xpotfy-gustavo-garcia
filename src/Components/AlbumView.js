import React, { useState, useRef, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { sluglizer } from '../helpers'
import AlbumCover from './AlbumCover';
import SongCard from './SongCard';

export default function AlbumView({ albumCover, albumName, artistName, albumSongs }) {
  let history = useHistory();
  const player = useRef(null);
  const [selectedTrack, setSelectedTrack] = useState('');
  const [playerStatus, setplayerStatus] = useState('stoped');
  const playerObj = { player, playerStatus, selectedTrack, setSelectedTrack};

  useEffect(() => {
    player.current.src = selectedTrack;
    selectedTrack && player.current.play();
  }, [selectedTrack])

  return (
    <div className="albumView">
      <div className="albumInfo">
        <AlbumCover albumCover={albumCover} />
        <h3 className="albumName">{albumName}</h3>
        <button className="artistName" onClick={() => history.push(`/albums/${sluglizer(artistName)}`)}>
          <h4 className="artistName">{artistName}</h4>
        </button>
      </div>
      <ul className="albumSongs">
        {albumSongs.map((songInfo) => <SongCard key={songInfo.songID} {...songInfo} { ...playerObj } />)}
      </ul>
      <audio 
        ref={player} 
        onPlay={() => setplayerStatus('playing')} 
        onPause={() => setplayerStatus('stoped')} 
      />
    </div>
  )
} 