import React from 'react';
import { useHistory } from "react-router-dom";
import { useTranslation } from 'react-i18next';

export default function AlbumCard({ imgSrc, albumName, artistName }) {
  const { t } = useTranslation();
  let history = useHistory();

  return (
    <button className="backButton" onClick={() => history.goBack()}>{t('APP.BACK')}</button>
  )
} 