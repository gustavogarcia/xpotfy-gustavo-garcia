export const sluglizer = text => {
  return text
    .normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    .replace(/([^\w]+|\s+)/g, '-')
    .replace(/\-\-+/g, '-')
    .replace(/(^-+|-+$)/, '')
    .toLowerCase();
}

export const desluglizer = text => {
  return text.replace(/\-/g, " ")
}

export const goToPage = (destination, artistName, albumName) => {
  const goToAlbum = destination === "album"
  return `/albums/${sluglizer(artistName)}${goToAlbum ? `/${sluglizer(albumName)}` : ""}`
}

export const millisToMinutes = (millis) => {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
}

export function debounce(func, wait) {
  let timeout
  return function (...args) {
    const context = this
    clearTimeout(timeout)
    timeout = setTimeout(() => func.apply(context, args), wait)
  }
}

export const getToken = () => {
    const token = window.location.hash
    .substring(1)
    .split('&')
    .reduce((result, item) => {
      var parts = item.split('=');
      result[parts[0]] = parts[1];
      return result;
    }, {})["access_token"]
    window.localStorage.setItem('token', token)
    return token
}