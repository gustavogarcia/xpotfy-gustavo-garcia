import React, { useEffect } from 'react';
import BackButton from "../Components/BackButton";
import AlbumView from '../Components/AlbumView';
import { useDispatch, useSelector } from 'react-redux';
import { getAlbum, unsetAlbum } from '../store/actions/musicActions';
import { useLocation } from 'react-router-dom'

export default function AlbumPage() {
  const dispatch = useDispatch();
  const location = useLocation();
  const music = useSelector(state => state.music);
  useEffect(() => {
    dispatch(getAlbum(location.state.albumID));
    return dispatch(unsetAlbum());
  }, []);

  return (
    <>
      <BackButton />
      {music.album && <AlbumView {...music.album} />}
    </>
  )
} 