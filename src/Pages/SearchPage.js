import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useLocation } from "react-router-dom";
import { useTranslation } from 'react-i18next';
import { desluglizer, debounce } from '../helpers';
import { searchAlbums, searchMoreAlbums, unsetAlbums } from '../store/actions/musicActions';
import SquareList from '../Components/SquareList';
import Field from '../Components/Field';

export default function SearchPage() {
  const { artist } = useParams();
  const location = useLocation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const music = useSelector(state => state.music);
  const [searching, setSearching] = useState(null);
  const [query, setQuery] = useState(artist ? desluglizer(artist) : '');
  const [searchedMore, setSearchedMore] = useState(1);
  const recentSearch = JSON.parse(window.localStorage.getItem('recentSearch'));
  const recentOpen = JSON.parse(window.localStorage.getItem('recentOpen'));

  useEffect(() => { setQuery(artist ? desluglizer(artist) : '') }, [location]);
  useEffect(() => { clearTimeout(searching); setSearching(setTimeout(() => { query && dispatch(searchAlbums(query))}, 1000))}, [query]);
  useEffect(() => { return () => dispatch(unsetAlbums())}, []);

  window.onscroll = debounce(() => {
    const scrollOnBottom = window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight
    if (scrollOnBottom && query) {
      dispatch(searchMoreAlbums(query, searchedMore))
      setSearchedMore(searchedMore + 1)
    }
  }, 100);

  return (
    <>
      <Field className={`${ query ? "" : "fieldEmpty"}`} label="SEARCH.LABEL" placeholder="SEARCH.PLACEHOLDER" value={query} onChange={setQuery} />
      {query
        ? <SquareList label="LIST.QUERYED" albums={music.searchedAlbums} query={query} />
        : <>
          { recentSearch && <SquareList label="LIST.RECENTLY_SEARCHED" albums={recentSearch} />}
          { recentOpen && <SquareList label="LIST.RECENTLY_OPENED" albums={recentOpen} />}
        </>
      }
    </>
  );
} 