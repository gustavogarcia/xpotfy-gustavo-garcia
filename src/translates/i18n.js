import i18next from 'i18next';
import translatePT from './translatePT'
import translateEN from './translateEN'

i18next
  .init({
    interpolation: {
      escapeValue: false,
    },
    lng: 'pt',
    resources: {
      pt: translatePT,
      en: translateEN
    },
  })

export default i18next