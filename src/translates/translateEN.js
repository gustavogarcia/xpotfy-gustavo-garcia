const translateEN = {
  translation: {
    APP: {
      BACK: 'Back',
    },
    SEARCH: { 
      LABEL: 'Search for artists, albums, and musics',
      PLACEHOLDER: 'Start writing...',
    },
    LIST: {
      QUERYED: "Results found for",
      RECENTLY_SEARCHED: "Recently Searched Albums",
      RECENTLY_OPENED: "Recently Searched Albums",
      NO_SEARCHED: "No Recently Searched Albums",
      NO_OPENED: "No albums recently opened",
    }
  }
}

export default translateEN