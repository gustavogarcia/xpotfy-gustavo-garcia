const translatePT = {
  translation: {
    APP: {
      BACK: 'Voltar',
    },
    SEARCH: { 
      LABEL: 'Busque por artistas, albuns ou músicas',
      PLACEHOLDER: 'Comece a escrever...',
    },
    LIST: {
      QUERYED: "Resultados encontrados para",
      RECENTLY_SEARCHED: "Álbuns buscados recentemente",
      RECENTLY_OPENED: "Álbuns abertos recentemente",
      NO_SEARCHED: "Nenhum album buscado recentemente",
      NO_OPENED: "Nenhum album aberto recentemente",
    }
  }
}

export default translatePT