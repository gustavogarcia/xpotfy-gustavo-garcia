import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import Logo from './Components/Logo';
import SearchPage from './Pages/SearchPage';
import AlbumPage from './Pages/AlbumPage';
import { authorizeAPI, refreshToken } from './store/actions/musicActions';
import { Flags } from './Components/Flags';
import { getToken } from './helpers';

export default function Routes() {
  const [token] = useState(window.localStorage.getItem('token') || getToken(window.location))
  console.log()
  useState(() => {
    token ? refreshToken()   : authorizeAPI()
    window.location.location = "";
  }, [])

  return (
    <>
      <Logo />
      <Flags />
        <div className="container">
        <Switch>
          <Route path="/albums/:artist/:album" component={AlbumPage} />
          <Route path="/albums/:artist" component={SearchPage} />
          <Route path="/" component={SearchPage} />
        </Switch>
      </div>
    </>
  )
}